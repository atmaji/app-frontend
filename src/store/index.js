import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { reducer as formReducer } from 'redux-form';

import loginReducer from 'features/login/duck';
import homeReducer from 'features/home/duck';

export default initial => (
    applyMiddleware(thunk)(createStore)(
        combineReducers({
            form: formReducer,
            login: loginReducer,
            home: homeReducer,
        }),
        localStorage['redux-store'] ?
            JSON.parse(localStorage['redux-store']) :
            initial,
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )
);