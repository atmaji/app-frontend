import React from 'react';
// import Icon from '@mdi/react';
// import { mdiAlertCircleOutline } from '@mdi/js';

const TextInput = ({ meta, input, callback, placeholder, isPassword = false, disabled, label, className, display = '', hidden = false, ...field }) => (
    <div className="form-group custom-text">
        {label && <label className={`control-label mb-1 ${meta.active ? 'focus' : ''}`}>
            {`${label} ${meta.error ? '*' : ''}`}
        </label>}
        <input
            {...input}
            onChange={e => { input.onChange(e); callback && callback(); }}
            placeholder={placeholder}
            type={`${isPassword ? 'password' : 'text'}`}
            className={`form-control ${meta.touched && meta.error ? 'error' : ''} mb-0 ${className ? className : ''}`}
            disabled={disabled}
            style={{ display }}
            maxLength={field.maxLength}
            minLength={field.minLength}
            hidden={hidden} />
        {
            meta.touched &&
            meta.error &&
            <small className="help-block form-text d-flex align-items-center pt-1" style={{ color: '#FF6258' }}>
                {/* <Icon path={mdiAlertCircleOutline} className="mr-2" style={{ position: 'static' }} size={.7} color="#F72012" /> <i>{meta.error}</i> */}
            </small>
        }
    </div>
);

export default TextInput;