import { expandable } from 'utils/hoc';

const Accordion = ({ children, isOpen, onToggle, onSetWrapper }) => (
    children(isOpen, onToggle, onSetWrapper)
);

export default expandable(Accordion);