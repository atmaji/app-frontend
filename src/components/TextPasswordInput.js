import React, { Component } from 'react';
// import Icon from '@mdi/react';
// import { mdiAlertCircleOutline } from '@mdi/js';
class TextPasswordInput extends Component {
    state = {
        isPasswordShown: false
    }

    togglePasswordVisibility = () => {
        const { isPasswordShown } = this.state;
        this.setState({ isPasswordShown: !isPasswordShown });

    }
    render() {
        const { isPasswordShown } = this.state;
        const { meta, input, callback, placeholder, disabled, label, className, display = '', ...field } = this.props;
        return (
            <div className="form-group custom-text">
                {label && <label className={`control-label mb-1 ${meta.active ? 'focus' : ''}`}>
                    {`${label} ${meta.error ? '*' : ''}`}
                </label>}
                <div className="input-group">
                    <input
                        {...input}
                        onChange={e => { input.onChange(e); callback && callback(); }}
                        placeholder={placeholder}
                        type={isPasswordShown ? "text" : "password"}
                        className={`form-control ${meta.touched && meta.error ? 'error' : ''} mb-0 ${className ? className : ''}`}
                        disabled={disabled}
                        style={{ display }}
                        maxLength={field.maxLength}
                        minLength={field.minLength} />
                    <div className="input-group-append">
                        <i className={`fa ${isPasswordShown ? "fa-eye" : "fa-eye-slash"} password-icon input-group-text`}
                            onClick={this.togglePasswordVisibility} />
                    </div>
                </div>
                {
                    meta.touched &&
                    meta.error &&
                    <small className="help-block form-text d-flex align-items-center pt-1" style={{ color: '#FF6258' }}>
                        {/* <Icon path={mdiAlertCircleOutline} className="mr-2" style={{position: 'static'}} size={.7} color="#F72012" /> <i>{meta.error}</i> */}
                    </small>
                }
            </div>
        );
    }
}
export default TextPasswordInput;