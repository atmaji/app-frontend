import { TextInput, TextPasswordInput } from 'components';
import React, { useEffect } from 'react';
import { Field } from 'redux-form';

const LoginComponent = ({ handleSubmit, onLogin, history, cookies }) => {

    useEffect(() => {
        cookies.get('id') && history.push('/');
    })

    const onClickLogin = value => {
        onLogin(value.username, value.password).then(res => res && history.push('/'));
    }

    return (
        <div className='container-fluid'>
            <div className='row login'>
                <div className='col-md-3 content'>
                    <div className='row form'>
                        <h1 className='header pb-2'>Login To System Test</h1>
                        <p style={{ marginBottom: '92px' }}>This is a secure site. Please enter your login information to enter the system.</p>
                        <form onSubmit={handleSubmit(onClickLogin)}>
                            <Field name="username" label="Username" component={TextInput} />
                            <Field name="password" label="Password" component={TextPasswordInput} />
                            <button className='btn-outline-secondary' type='submit'>Login</button>
                        </form>
                    </div>
                </div>
                <div className='col-md-9 side'>
                </div>
            </div>
        </div>
    )
};

export default LoginComponent;