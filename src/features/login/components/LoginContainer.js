
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { reduxForm } from 'redux-form';
import LoginComponent from './LoginComponent';
import { loginOperations } from '../duck'; 
import { withCookies } from 'react-cookie';

const mapStateProps = state => ({
});

const mapDispatchProps = {
    onLogin: loginOperations.login
};

export default withRouter(
    connect(mapStateProps, mapDispatchProps)(
        reduxForm({
            form: 'login'
        })(withCookies(LoginComponent)))
);