import actions from './actions';
import api from 'utils/api';
import { Cookies } from 'react-cookie';

const setCookies = obj => {
    const cookies = new Cookies();
    const keys = Object.keys(obj);
    const date = new Date();
    const expireDate = date.getTime() + (1 * 1000 * 60 * 60 * 24);
    keys.forEach(key => {
        cookies.set(key, obj[key], { path: '/', expires: new Date(expireDate) });
    });
}

const login = (username, password) => (
    dispatch => {
        dispatch(actions.requestLogin());
        return api.postAuth('/user-management/login', username, password)
            .then(data => {
                console.log(data);
                setCookies({...data.data, ...data.token, ...data.dataRole});
                dispatch(actions.successLogin());
                return true;
            })
            .catch(error => {
                dispatch(actions.errorLogin(error.message.message));
                return false;
            });
    }
);

export default {
    login
}