import React from 'react';
import NavLinkComponent from './NavLinkComponent';

const MenuComponent = ({ menus }) => (
    <ul className="menu nav flex-column">
        {console.log(menus)}
        {menus.parents.map(parent =>
            <NavLinkComponent
                menus={menus}
                child={menus.children.filter(children => children.vparent === parent.vparent)}
                key={parent.vmenuid}>
                {parent.vnamemenu}
            </NavLinkComponent>
        )}
    </ul>
);

export default MenuComponent;