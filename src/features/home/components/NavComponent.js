import React from 'react';
import { Accordion } from 'components';
import { Cookies } from 'react-cookie';
import { mdiForwardburger, mdiChevronDown } from "@mdi/js";
import Icon from "@mdi/react";

const cookies = new Cookies();

const line = {
  color: "white",
  borderRight: "2px solid white",
  height: "50px",
  paddingRight: "27px",
  marginRight: "27px"
};

const NavComponent = ({ setIsLogout, onToggle, isOpen }) => {

    const logOut = () => {
        const keys = Object.keys(cookies.getAll())
        keys.forEach(key => {
            cookies.remove(key);
        })
        setIsLogout(true)
    }

    return (
        <div className="navigation">
            <div className="container-fluid wrapper" >
                <div className="col-md-3 left">
                    <Icon
                        path={mdiForwardburger}
                        onClick={onToggle}
                        size={1.5}
                        rotate={!isOpen ? 180 : 0}
                        color="#FFFFFF"
                    />
                </div>
                <div className="col-md-9 right">
                    <Accordion>
                        {(isOpen, onToggle, onSetWrapper) => (
                            <div className="user" onClick={onToggle} ref={onSetWrapper}>
                                {isOpen ? (
                                    <div className="panel">
                                        <ul>
                                            <li onClick={logOut}>Log out</li>
                                        </ul>
                                    </div>
                                ) : null}
                                <span className="name">{cookies.get("vfullname")},</span>
                                <span className="title">{cookies.get("role")}</span>
                            </div>
                        )}
                    </Accordion>
                    <div style={line}></div>
                    <div className="language">
                        <span>Eng</span>
                        <Icon path={mdiChevronDown} size={0.8} color="#FFFFFF" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NavComponent;