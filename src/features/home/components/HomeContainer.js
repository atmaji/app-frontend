import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withCookies } from "react-cookie";
import HomeComponent from "./HomeComponent";
import { homeOperations, homeSelectors } from "../duck";

const mapStateProps = state => {
    return {
        tabs: state.home.tab,
        activeTab: homeSelectors.activeTab(state),
        // loading: homeSelectors.loading(state),
        // message: homeSelectors.message(state),
        menus: {
            parents: homeSelectors.parentMenu(state),
            children: homeSelectors.childMenu(state)
        }
    }
};

const mapDispatchProps = {
    getMenu: homeOperations.getMenu
};

export default withRouter(
    connect(
        mapStateProps,
        mapDispatchProps
    )(withCookies(HomeComponent))
);
