import React, { useEffect, useState } from 'react'
import NavComponent from './NavComponent';
import { Accordion } from "components";
import { Fragment } from 'react';
import MenuComponent from "./MenuComponent";

const HomeComponent = ({ cookies, history, getMenu, menus }) => {
    const [isLogout, setIsLogout] = useState(false);
    const [isGetMenu, setIsGetMenu] = useState(false);

    useEffect(() => {
        isLogout && !cookies.get("id") && history.push("/login");
        if (!isGetMenu) {
            getMenu({ vid_uam_mstusrrole: cookies.get("idusrrole") })
                .then(res => {
                    if (res) {
                        setIsGetMenu(true)
                    }
                })
        }
    })

    return (
        <div className="home">
            <Accordion>{(isOpen, onToggle) => (
                <Fragment>
                    <NavComponent
                        setIsLogout={setIsLogout}
                        isOpen={isOpen}
                        onToggle={onToggle} />
                    <div className="d-flex justify-content-start">
                        {!isOpen && (
                            <MenuComponent
                                menus={menus}
                            // activeTab={activeTab}
                            // addTab={addTab}
                            />
                        )}
                        <div className="content" style={{ width: !isOpen ? "calc(100vw - 265px)" : "100vw" }}>

                        </div>
                    </div>
                </Fragment>
            )}</Accordion>


        </div>
    )
}

export default HomeComponent