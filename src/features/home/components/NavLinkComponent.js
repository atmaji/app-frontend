import React from "react";
import Icon from "@mdi/react";
import { mdiChevronDown } from "@mdi/js";
import { Accordion } from "components";

const NavLinkComponent = ({ children, child, addTab, activeTab }) => {
  const activeTabId = activeTab && activeTab.id;

  return (
    <Accordion>
      {(isOpen, onToggle, onSetWrapper) => (
        <div ref={onSetWrapper}>
          <li
            className={`nav-link ${isOpen ? "active" : ""}`}
            onClick={onToggle}
          >
            {children}
            <Icon
              path={mdiChevronDown}
              className="float-right"
              rotate={isOpen ? 0 : -90}
              size={1}
              color="#FFFFFF"
            />
          </li>
          <ul className={`children nav flex-column ${isOpen ? "" : "d-none"}`}>
            {child.map(data => (
              <li
                className={`nav-link ${
                  activeTabId === data.id ? "active" : ""
                  }`}
                // onClick={() => addTab(data)}
                key={data.vmenuid}
              >
                {data.vnamemenu}
              </li>
            ))}
          </ul>
        </div>
      )}
    </Accordion>
  );
};

export default NavLinkComponent;
