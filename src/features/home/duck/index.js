import reducer from './reducers';
import homeOperations from './operations';
import homeSelectors from './selectors';

export { homeOperations, homeSelectors };

export default reducer;