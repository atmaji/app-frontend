import { createSelector } from 'reselect';
import _ from 'lodash';

const tabSelector = state => state.home.tab;

const menuSelector = state => state.home.menu;

const commonSelector = state => state.home.common;

const activeTab = createSelector(
    [tabSelector, menuSelector],
    (tabs, menu) => {
        const tab = tabs.find(tab => tab.isActive);
        const parent = tab && menu.data.find(menu => menu.id === tab.parent);
        return parent ? { ...tab, parent } : tab;
    }
);

const parentMenu = createSelector(
    menuSelector,
    ({ data }) => data.filter(menu => menu.bisparent === true)
);

const childMenu = createSelector(
    menuSelector,
    ({ data }) => data.filter(menu => menu.bisparent === false)
);

const loading = createSelector(
    menuSelector,
    commonSelector,
    (menu, common) => menu.loading || common.loading
);

const message = createSelector(
    menuSelector,
    commonSelector,
    (menu, common) => menu.message === common.message ? menu.message : _.compact([menu.message, common.message]).join('. ')
)

export default {
    activeTab,
    parentMenu,
    childMenu,
    loading,
    message,
};