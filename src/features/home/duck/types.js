const REQUEST_GET_MENU = 'login/REQUEST_GET_MENU';
const SUCCESS_GET_MENU = 'login/SUCCESS_GET_MENU';
const ERROR_GET_MENU = 'login/ERROR_GET_MENU';

const ADD_TAB = 'home/ADD_TAB';
const REMOVE_TAB = 'home/REMOVE_TAB';
const SELECT_TAB = 'home/SELECT_TAB';

export default {
    REQUEST_GET_MENU,
    SUCCESS_GET_MENU,
    ERROR_GET_MENU,
    ADD_TAB,
    REMOVE_TAB,
    SELECT_TAB,
}