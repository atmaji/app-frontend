import types from './types';
import { combineReducers } from "redux";

const menuReducer = (state = {
    loading: false,
    data: [],
    message: ''
}, action) => {
    switch (action.type) {
        case types.REQUEST_GET_MENU:
            return { loading: true, message: '', data: [] };
        case types.SUCCESS_GET_MENU:
            return { loading: false, message: '', data: action.menus };
        case types.ERROR_GET_MENU:
            return { loading: false, message: action.message, data: [] };
        default:
            return state;
    }
};

const tabsReducer = (state = [], action) => {
    switch (action.type) {
      case types.ADD_TAB:
        return !state.find(tab => tab.id === action.tab.id)
          ? [
            ...state.map(tab => ({ ...tab, isActive: false })),
            tabReducer({}, action)
          ]
          : [...state.map(tab => {
            return { ...tab, isActive: tab.id === action.tab.id ? true : false }
          })];
      case types.SELECT_TAB:
        return state.map(tab => tabReducer(tab, action));
      case types.REMOVE_TAB:
        const activeIndex =
          state.findIndex(tab => tab.id === action.id) !== 0
            ? state.findIndex(tab => tab.id === action.id) - 1
            : 0;
  
        const deletedTabs = state.filter(tab => tab.id !== action.id);
        const isTabActive = deletedTabs.findIndex(tab => tab.isActive === true);
  
        return action.id
          ? deletedTabs.map((tab, index) =>
            isTabActive !== -1
              ? tab
              : index === activeIndex
                ? { ...tab, isActive: true }
                : { ...tab, isActive: false }
          )
          : [];
      case types.CHANGE_PAGE:
        return state.map(tab => tabReducer(tab, action));
      default:
        return state;
    }
  };
  
  const tabReducer = (state = {}, action) => {
    switch (action.type) {
      case types.ADD_TAB:
        return {
          ...action.tab,
          isActive: true,
          pages: [{ id: "index", title: action.tab.title }]
        };
      case types.SELECT_TAB:
        return state.id !== action.id
          ? { ...state, isActive: false }
          : { ...state, isActive: true };
      case types.CHANGE_PAGE:
        return state.menuid === action.idTab
          ? { ...state, pages: pageReducer(state.pages, action) }
          : state;
      default:
        return state;
    }
  };

  const pageReducer = (state = [], action) => {
    switch (action.type) {
      case types.CHANGE_PAGE:
        if (action.id) {
          return state.find(page => page.id === action.id)
            ? state.slice(0, state.findIndex(page => page.id === action.id) + 1)
            : [
              ...state,
              { id: action.id, title: action.title, payload: action.payload }
            ];
        } else {
          return [{ id: "index" }];
        }
      default:
        return state;
    }
  };

const reducers = combineReducers({
    menu: menuReducer,
    tab: tabsReducer,
  });

export default reducers;