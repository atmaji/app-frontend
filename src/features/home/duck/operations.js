import actions from './actions';
import api from 'utils/api';
import types from './types';

const getMenu = (data) => (
    dispatch => {
        dispatch(actions.requestGetMenu());
        return api.post('/menu/get-menu', data)
            .then(res => {
                const menus = res.map(obj => obj.uam_mstmenu)
                dispatch(actions.successGetMenu(menus))
                return true
            })
            .catch(err => {
                dispatch(actions.errorGetMenu(err))
                return false
            })
    }
)

const addTab = tab => (
    dispatch => {
        dispatch({
            type: types.ADD_TAB,
            tab
        });
    }
);

const removeTab = id => (
    dispatch => {
        dispatch({
            type: types.REMOVE_TAB,
            id
        });
    }
);

const selectTab = id => (
    dispatch => {
        dispatch({
            type: types.SELECT_TAB,
            id
        });
    }
);

export default {
    getMenu,
    addTab,
    removeTab,
    selectTab
}