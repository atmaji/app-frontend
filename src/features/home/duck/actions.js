import types from './types';

const requestGetMenu = () => ({
    type: types.REQUEST_GET_MENU
});

const successGetMenu = (menus) => ({
    type: types.SUCCESS_GET_MENU,
    menus
});

const errorGetMenu = message => ({
    type: types.ERROR_GET_MENU,
    message
});

export default {
    requestGetMenu,
    successGetMenu,
    errorGetMenu
}