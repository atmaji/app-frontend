import React, { Component, Fragment } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom'
import LoginContainer from 'features/login';
import HomeContainer from 'features/home';

class AppComponent extends Component {
    render() {
        return (
            <HashRouter>
                <Fragment>
                    <Switch>
                        <Route exact path="/" component={HomeContainer} />
                        <Route path="/login" component={LoginContainer} />
                    </Switch>
                </Fragment>
            </HashRouter>
        );
    }
}

export default AppComponent