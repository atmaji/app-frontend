import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import api from 'utils/api';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'assets/vendor/fontawesome/css/all.css';
import 'assets/css/main.scss';
import AppContainer from 'features/app';
import { CookiesProvider } from 'react-cookie';

api.setupInterceptors(store());

ReactDOM.render(
    <CookiesProvider>
        <Provider store={store()}>
            <AppContainer />
        </Provider>
    </CookiesProvider>,
    document.getElementById('root')
);

